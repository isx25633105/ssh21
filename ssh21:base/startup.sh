#! /bin/bash
useradd -m -s /bin/bash unix01
useradd -m -s /bin/bash unix02
useradd -m -s /bin/bash unix03
echo -e "unix01\nunix01" | passwd unix01
echo -e "unix02\nunix02" | passwd unix02
echo -e "unix03\nunix03" | passwd unix03

cp /opt/docker/ldap.conf /etc/ldap/
cp /opt/docker/nsswitch.conf /etc/
cp /opt/docker/nslcd.conf /etc/
cp /opt/docker/common-session /etc/pam.d/
cp /opt/docker/common-auth /etc/pam.d/
cp /opt/docker/common-password /etc/pam.d/
cp /opt/docker/common-account /etc/pam.d/
cp /opt/docker/sshd_config  /etc/ssh/sshd_config
mkdir root/.ssh
cp /opt/docker/authorized_keys /root/.ssh/
mkdir /run/sshd
/usr/sbin/nslcd  
/usr/sbin/nscd 	
/usr/sbin/sshd	-D
./ldapusers.sh
/bin/bash
