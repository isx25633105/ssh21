# PAM @isx25633105 ASIX
# Curs 2021-2022

* **isx25633105/ssh21:base:**
* Ordre per executar el container:
* Xarxa propia:

```

docker build -t isx25633105/ssh21:base .

docker run --privileged --rm --name ssh.edt.org -h ssh.edt.org --net 2hisx -p 2022:22 -it isx25633105/ssh21:base /bin/bash
```

S'ha modificat la versio de pam base per poder entrar per ssh amb keys i identificarse en una container ldap (isx25633105/ldap21:group).

Atenció:
Aquest container a sigut modificar per conectarse amb unes keys. En cas de no poder conectar amb aquestes keys, crea unes noves i enviala al container.
 
